<a name="intro">Getting started</a>
-----------------------------------

### Why `biblatex`

- Works with `biber`, so UTF-8 support is provided.
- Aliases for bibliography entries are possible.
- More field, preprocessing, and customization options.

### Switching to `biblatex`

First of all, since for most paper the ACM templates are used, and these templates use
`natbib`, we need to disable that with the following line:

    \documentclass[natbib=false]{acmart}

Then, converting from `natbib` is pretty straightforward. The minimal requirements are:

    \usepackage[natbib=true]{biblatex}
    \bibliography{dotbibfile}

or better yet:

    \let\citename\relax
    \usepackage[natbib=true, abbreviate=true, dateabbrev=true, isbn=true, doi=true, urldate=comp, url=true, maxbibnames=9, maxcitenames=2, backref=false, backend=biber, style=ACM-Reference-Format, language=american]{biblatex}
    \addbibresource{../bib/biblio.bib}
    \renewcommand{\bibfont}{\Small}

 We can add one or several local `.bib` files
as our database using the `\addbibresource` command. In addition,
`\addbibresource` also allows to load remote resources and other data
types (e.g., `ris`).

Finally, to actually print the bibliography, we can use the following command at the place
where we want it to be printed:

    \printbibliography


<a name="usage">Usage</a>
-----------------------------------

### Citation commands

The option `natbib=true` enables `natbib` compatibility in order for
us to be able to use `\citep` and `\citet` as with `natbib`. It will
thus automatically create the relevant aliases for these commands, and
we can use them as before.

If `natbib` compatibility is not desired, we can use `\textcite`
instead of `\citet` and `\autocite` instead of `citep`.

Note that `\autocite` is preferred instead of `\parencite` (the actual
counterpart of `natbib`'s `\citep`), because `\autocite` is a
high-level citation command that will be translated into the low-level
bare citation command appropriate for the chosen style. For example,
it will enclose a citation in parentheses in author-year styles, but
produce a footnote citation in author-title styles. Furthermore, it
will automatically move trailing punctuation.

####  An Example

With `natbib`, a model LaTeX document would look as follows:

    \documentclass[<ACMoptions>]{acmart}

    \bibliographystyle{<somestyle>}

    \begin{document}

    A bare citation command: \citep{<key>}.
    A citation command for use in the flow of text: As \citet{<key>} said \ldots

    \bibliography{<mybibfile>} % Selects .bib file AND prints bibliography

    \end{document}

With `biblatex` and its built-in styles, this changes to:

    \documentclass[<ACMoptions>,natbib=false]{acmart}

    \let\citename\relax
    \usepackage[natbib=true, abbreviate=true, dateabbrev=true, isbn=true, doi=true, urldate=comp, url=true, maxbibnames=9, maxcitenames=2, backref=false, backend=biber, style=ACM-Reference-Format, language=american]{biblatex}

    % \bibliography{<mybibfile>}       % ONLY selects .bib file; syntax for version <= 1.1b
    \addbibresource{../bib/biblio.bib} % Syntax for version >= 1.2
    \renewcommand{\bibfont}{\Small}

    \begin{document}

    A bare citation command: \citep{<key>}.

    A citation command for use in the flow of text: As \citet{<key>} said \ldots

    \balance
    \printbibliography[<options for printing>]

    \end{document}

### Customization

#### Entry aliases

With `biblatex` an entry can have multiple aliases specified in the
`ids` field.

#### Title Casing

See [here](https://tex.stackexchange.com/questions/20335/proper-casing-in-citation-bibliography-titles-using-biblatex-biber)

#### Changing how words/phrases appear in the bibliography

For instance, one might want to replace "Lecture Notes in Computer Science"
by "LNCS" and "Lecture Notes In Artificial Intelligence" by "LNAI":

\DeclareSourcemap{
  \maps[datatype=bibtex]{
    \map{
      \step[fieldsource=series,
        match=\regexp{Lecture\s+Notes\s+in\s+Computer\s+Science},
        replace={LNCS}]
      \step[fieldsource=series,
        match=\regexp{Lecture\s+Notes\s+in\s+Artificial\s+Intelligence},
        replace={LNAI}]
    }
  }
}

#### Creating appendix entries

To avoid creating an appendix entry for each different paper, an
`appendix` key is provided to make easier referring to appendices.

To change the text that appears for the appendix, you can set
appropriately the `\bibAppendix` and `\bibAppendixAuthor` macros in
the preamble.

#### Changing venue names

In the beginning of the `.bib` file there are macros defined with
names for all conferences and journals used in the bibliography. These
macros can be changed accordingly depending on the desired publishing
style.

#### Suppressing a particular field

Instead of having to manually edit the bibliography, with `biblatex` one
can put a command like the following in the preamble, in order to suppress, e.g.,
the `note` field:

    \AtEveryBibitem{%
      \clearfield{note}%
    }

These [guidelines](https://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles/13076#13076) are extremely useful for customizing the appearance
of a bibliography.


<a name="problems">Troubleshooting</a>
-----------------------------------

### Citation commands do not work properly

If some previous version of the bibliography file had been compiled
using `natbib`, you may need to delete some of the auxiliary files
created by LaTeX and BibTeX (`.aux`, `.bbl`, `.blg`) for it to work
properly.

### Balancing the bibliography

If the bibliography does not turn out balanced, use a `\balance`
command just above the `printbibliography` to make it balanced.

### The names of all the authors are printed when citing a paper

By default, if there are more than one papers by the same authors,
`biblatex` will print the full names when using `\citet`, to avoid
ambiguity in text. This is why the `maxcitenames=2` option is necessary.
