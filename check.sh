#!/bin/bash

# Checks the format of biblio.m4
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, you can access it online at
# http://www.gnu.org/licenses/gpl-2.0.html.
#
# Author: Michalis Kokologiannakis <mixaskok@gmail.com>

# current directory
TOP="${TOP:-$PWD}"

input=$(cat $TOP/biblio.m4)
sorted=$($TOP/sort.sh /dev/stdout)

# check whether input is sorted
if [[ "${input}" != "${sorted}" ]]
then
    diff <(echo "${input}") <(echo "${sorted}")
    echo "Input not sorted! Consider running 'sort.sh'"
    exit 1
fi

# check whether venue names are properly set
errors=$(cat biblio.m4 | awk '
    		BEGIN { proceedings=0; article = 0; }
		/@/   { if ($1 ~ /@Article/) article = 1; else article = 0;
		      	if ($1 ~ /Proceedings/) proceedings = 1; else proceedings = 0; }
	        /booktitle/ { if (proceedings && $3 !~ /CONF_*/) print $0 }
		/series/ { if (article && $3 !~ /JNL_*/) print $0 }'
      )
if [[ -n "${errors}" ]]
then
    echo "${errors}"
    echo "Entries not properly set! Consider using CONF_* and JNL_*."
    exit 2
fi
